package com.mojix.java_android_coway_wtms_mobile.rxJava;

public class RxEventItem {
    public String sender;
    public Object obj;

    public RxEventItem( String sender, Object obj ){
        this.sender = sender;
        this.obj = obj;
    }
}

