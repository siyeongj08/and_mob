package com.mojix.java_android_coway_wtms_mobile.rxJava.utils;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.ActivityCompat;

public class GpsTracker extends Service implements LocationListener {

    double latitude = 0;
    double longitude = 0;
    protected LocationManager locationManager;
    private static GpsTracker gpsTracker ;

    private GpsTracker(){

    }

    public static GpsTracker getInstance() {
        if (gpsTracker == null) {
            gpsTracker = new GpsTracker();
        }
        return gpsTracker;
    }

    public void initGPS(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);

        if(locationManager != null) {
            // GPS 프로바이더 사용가능여부
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            }

            // 네트워크 프로바이더 사용가능여부
            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            }
        }
    }

    public double getLatitude()
    {
        return latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    @Override
    public void onLocationChanged(Location location)
    {
        latitude = location.getLatitude();   //위도
        longitude = location.getLongitude(); //경도

        Log.i("YS", "onLocationChanged : " + latitude + ", " + longitude);
        Log.d("LocationListener", "longitude=" + longitude + " latitude=" + latitude);
    }

    @Override
    public void onProviderDisabled(String provider)
    {
        Log.d("LocationListener", "onProviderDisabled, provider:" + provider);
    }

    @Override
    public void onProviderEnabled(String provider)
    {
        Log.d("LocationListener", "onProviderEnabled, provider:" + provider);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {
        Log.d("LocationListener", "onStatusChanged, provider:" + provider + ", status:" + status + " ,Bundle:" + extras);
    }

    @Override
    public IBinder onBind(Intent arg0)
    {
        return null;
    }


    public void stopUsingGPS()
    {
        if(locationManager != null)
        {
            locationManager.removeUpdates(GpsTracker.this);
        }
    }
}
