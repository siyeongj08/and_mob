package com.mojix.java_android_coway_wtms_mobile.rxJava;

public class RxEventMapper {
    public static final String ClOSE_ACTIVITY = "ClOSE_ACTIVITY";
    public static final String myURL = "myURL";
    public static final String doScan = "doScan";
    public static final String webPageLoaded = "webPageLoaded";
    public static final String settingAppConst = "settingAppConst";
    public static final String getAppVersion = "getAppVersion";
    public static final String setStartURL ="setStartURL";
    public static final String insertManualData ="insertManualData";
}
