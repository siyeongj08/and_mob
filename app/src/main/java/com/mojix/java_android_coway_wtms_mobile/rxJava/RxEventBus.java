package com.mojix.java_android_coway_wtms_mobile.rxJava;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;

public class RxEventBus {

    private static RxEventBus rxEventBus ;
    private final PublishSubject<Object> publishSubject ;

    private RxEventBus(){
        publishSubject = PublishSubject.create();
    }

    public static RxEventBus getInstance() {
        if (rxEventBus == null) {
            rxEventBus = new RxEventBus();
        }

        return rxEventBus;
    }

    public void sendEvent(Object obj){
        publishSubject.onNext(obj);
    }

    public Observable<Object> getEvent()
    {
        return publishSubject ;
    }

}