package com.mojix.java_android_coway_wtms_mobile.webutils;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventBus;
import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventItem;
import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventMapper;

import java.net.URISyntaxException;

public class ExWebViewClient  extends WebViewClient {
    public static final String INTENT_URI_START = "intent:";
    public static final String INTENT_FALLBACK_URL = "browser_fallback_url";
    public static final String URI_SCHEME_MARKET = "market://details?id=";

    private boolean firstStart = true;
    private WebView mWebView;
    private WebView mChildView;
    private ProgressDialog mProgressDialog;
    private Context mContext;
    private String mChildURL;

    public ExWebViewClient (Context context, WebView webView, WebView childView, String childURL, ProgressDialog progressDialog){
        mWebView = webView;
        mChildView = childView;
        mContext = context;
        mChildURL = childURL;
        //mProgressDialog = progressDialog;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.toLowerCase().startsWith(INTENT_URI_START) || !url.toLowerCase().startsWith("http")) {
            Intent parsedIntent = null;
            try {
                parsedIntent = Intent.parseUri(url, 0);
                mContext.startActivity(parsedIntent);
            } catch(ActivityNotFoundException | URISyntaxException e) {
                return doFallback(view, parsedIntent);
            }
        } else {
            view.loadUrl(url);
        }
        return true;
    }

    private boolean doFallback(WebView view, Intent parsedIntent) {
        if (parsedIntent == null) {
            return false;
        }
        String fallbackUrl = parsedIntent.getStringExtra(INTENT_FALLBACK_URL);
        if (fallbackUrl != null) {
            view.loadUrl(fallbackUrl);
            return true;
        }
        String packageName = parsedIntent.getPackage();
        if (packageName != null) {
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URI_SCHEME_MARKET + packageName)));
            return true;
        }
        return false;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);

        RxEventBus.getInstance().sendEvent(new RxEventItem(RxEventMapper.myURL, url));
//        if (firstStart){
//            mProgressDialog.setTitle("화면 로딩 중..");
//            mProgressDialog.setMessage("Loading");
//            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//            mProgressDialog.setCancelable(true);
//            mProgressDialog.setCanceledOnTouchOutside(false);
//            mProgressDialog.show();
//        }

    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if(firstStart){
            if(mProgressDialog != null)
                mProgressDialog.dismiss();
            firstStart= false;
        }


        RxEventBus.getInstance().sendEvent(new RxEventItem(RxEventMapper.myURL, url));

        if(!mChildURL.equals("") || !mChildURL.equals(null) || !mChildURL.isEmpty()) {
            mChildURL = "";
            if(mWebView != null)
                mWebView.removeView(mChildView);
        }
    }
}
