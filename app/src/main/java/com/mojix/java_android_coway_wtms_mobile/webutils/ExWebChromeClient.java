package com.mojix.java_android_coway_wtms_mobile.webutils;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Message;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ViewGroup;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.mojix.java_android_coway_wtms_mobile.MainActivity;
import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventBus;
import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventItem;
import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventMapper;

import java.io.File;

import static android.app.Activity.RESULT_OK;
import static androidx.core.app.ActivityCompat.startActivityForResult;

public class ExWebChromeClient extends WebChromeClient {

    private WebView mWebView;
    private WebView mChildView;
    private Context mContext;
    private String mChildURL;

    public static ValueCallback<Uri> filePathCallbackNormal;
    public static ValueCallback<Uri[]> filePathCallbackLollipop;
    public final static int FILECHOOSER_NORMAL_REQ_CODE = 2001;
    public final static int FILECHOOSER_LOLLIPOP_REQ_CODE = 2002;
    public static Uri cameraImageUri = null;

    private int count = 1;
    public ExWebChromeClient (Context context, WebView webView, WebView childView, String childURL){
        mWebView = webView;
        mChildView = childView;
        mContext = context;
        mChildURL = childURL;
    }


    @Override
    public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {

        count = 1;
        mWebView.removeAllViews();

        mChildView = new WebView(mContext);
        mChildView.getSettings().setJavaScriptEnabled(true);
        mChildView.setWebChromeClient(this);

        mChildView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                mChildURL = url;

                if (count == 1) {
                    count = 0;
                    if (mChildURL.contains("웹브라우저로 띄우고싶은 url")) {
                        mWebView.removeView(mChildView);
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mChildURL));
                        mContext.startActivity(intent);
                        mChildURL="";
                    }
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                count = 1;
            }
        });

        mChildView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mWebView.addView(mChildView);
        WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
        transport.setWebView(mChildView);
        resultMsg.sendToTarget();
        return true;
    }

    @Override
    public void onCloseWindow(WebView window) {
        super.onCloseWindow(window);
    }

    @Override
    public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
        final JsResult finalRes = result;
        RxEventBus.getInstance().sendEvent(new RxEventItem(RxEventMapper.myURL, url));
        //AlertDialog 생성
        new AlertDialog.Builder(view.getContext())//, R.style.Theme_DeviceDefault_Light_Dialog_Alert )
                .setMessage(message)
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finalRes.confirm();
                    }
                })
                .setCancelable(false)
                .create()
                .show();
        return true;
    }

    @Override
    public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
        final JsResult finalRes = result;
        RxEventBus.getInstance().sendEvent(new RxEventItem(RxEventMapper.myURL, url));
        //AlertDialog 생성
        new AlertDialog.Builder(view.getContext())//, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                .setMessage(message)
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finalRes.confirm();
                    }
                })
                .setCancelable(false)
                .create()
                .show();
        return true;
    }

    @Override
    public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
        final JsResult finalRes = result;
        RxEventBus.getInstance().sendEvent(new RxEventItem(RxEventMapper.myURL, url));
        //AlertDialog 생성
        new AlertDialog.Builder(view.getContext())//, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
                .setMessage(message)
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finalRes.confirm();
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finalRes.cancel();
                    }
                })
                .setCancelable(false)
                .create()
                .show();
        return true;
    }

    // For Android 5.0+ 카메라 - input type="file" 태그를 선택했을 때 반응
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean onShowFileChooser(
            WebView webView, ValueCallback<Uri[]> filePathCallback,
            FileChooserParams fileChooserParams) {
        Log.d("MainActivity", "5.0+");

        // Callback 초기화 (중요!)
        if (filePathCallbackLollipop != null) {
            filePathCallbackLollipop.onReceiveValue(null);
            filePathCallbackLollipop = null;
        }
        filePathCallbackLollipop = filePathCallback;

        boolean isCapture = fileChooserParams.isCaptureEnabled();

        runCamera(isCapture);
        return true;
    }

    private void runCamera(boolean _isCapture)
    {
        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        File path = ((MainActivity)mContext).getFilesDir();
        File file = new File(path, "ceoStamp.png"); // sample.png 는 카메라로 찍었을 때 저장될 파일명이므로 사용자 마음대로
        // File 객체의 URI 를 얻는다.
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            String strpa = ((MainActivity)mContext).getApplicationContext().getPackageName();
            cameraImageUri = FileProvider.getUriForFile(((MainActivity)mContext), strpa , file); // + ".fileprovider"
        }
        else
        {
            cameraImageUri = Uri.fromFile(file);
        }
        intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri);

        if (_isCapture)
        { // 선택팝업 카메라, 갤러리 둘다 띄우고 싶을 때
            Intent pickIntent = new Intent(Intent.ACTION_PICK);
            pickIntent.setType(MediaStore.Images.Media.CONTENT_TYPE);
            pickIntent.setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            String pickTitle = "사진 가져올 방법을 선택하세요.";
            Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);

            // 카메라 intent 포함시키기..
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Parcelable[]{intentCamera});
            ((MainActivity)mContext).startActivityForResult(chooserIntent, FILECHOOSER_LOLLIPOP_REQ_CODE);
        }
        else
        {// 바로 카메라 실행..
            ((MainActivity)mContext).startActivityForResult(intentCamera, FILECHOOSER_LOLLIPOP_REQ_CODE);
        }
    }



}
