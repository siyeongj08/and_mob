package com.mojix.java_android_coway_wtms_mobile.webutils;

import android.os.Handler;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventBus;
import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventItem;
import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventMapper;

public class WebBridge {
    public WebBridge(WebView webView){
        mWebView = webView;
    }

    private String TAG = "AndroidBridge";
    final public Handler handler = new Handler();

    private WebView mWebView;

    // 로그를 띄우는 메소드 입니다.
    @JavascriptInterface
    public void call_log( final String _message ){
        Log.d(TAG, _message);
    }


    @JavascriptInterface
    public void doScan( final String _message ){
        RxEventBus.getInstance().sendEvent(new RxEventItem(RxEventMapper.doScan, _message));
    }

    @JavascriptInterface
    public void callAndroid(final String func, final String _message){
        RxEventBus.getInstance().sendEvent(new RxEventItem(func, _message));
    }

/*    @JavascriptInterface // 자바스크립트에서 보낸 함수
    public void setTitle( final String _message ){
        RxEventBus.getInstance().sendEvent(new RxEventItem("setTitle", _message));
    }*/

    public void sendScript(String javascript){ //안드로이드 -> 웹뷰
        handler.post(new Runnable() {
            @Override
            public void run() {
                mWebView.loadUrl(javascript); //"javascript:alert('안녕')"
            }
        });
    }

//    public void sendScript(String javascript){ //안드로이드 -> 웹뷰
//        handler.post(new scriptRunnable(javascript) {
//            @Override
//            public void run() {
//                mWebView.loadUrl(getJavascript()); //"javascript:alert('안녕')"
//            }
//        });
//    }
}
