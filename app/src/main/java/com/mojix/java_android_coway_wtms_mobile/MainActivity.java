package com.mojix.java_android_coway_wtms_mobile;


import static com.mojix.java_android_coway_wtms_mobile.webutils.ExWebChromeClient.FILECHOOSER_LOLLIPOP_REQ_CODE;
import static com.mojix.java_android_coway_wtms_mobile.webutils.ExWebChromeClient.FILECHOOSER_NORMAL_REQ_CODE;
import static com.mojix.java_android_coway_wtms_mobile.webutils.ExWebChromeClient.cameraImageUri;
import static com.mojix.java_android_coway_wtms_mobile.webutils.ExWebChromeClient.filePathCallbackLollipop;
import static com.mojix.java_android_coway_wtms_mobile.webutils.ExWebChromeClient.filePathCallbackNormal;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.zxing.Result;
import com.mojix.java_android_coway_wtms_mobile.camera.CameraUtil;
import com.mojix.java_android_coway_wtms_mobile.permission.MyPermission;
import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventBus;
import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventItem;
import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventMapper;
import com.mojix.java_android_coway_wtms_mobile.rxJava.utils.GpsTracker;
import com.mojix.java_android_coway_wtms_mobile.webutils.ExWebChromeClient;
import com.mojix.java_android_coway_wtms_mobile.webutils.ExWebViewClient;
import com.mojix.java_android_coway_wtms_mobile.webutils.WebBridge;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.observers.DisposableObserver;
import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class MainActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    public static final int REQUEST_PERMISSION_CODE = 1000;
    private DisposableObserver<? super Object> disposableObserver ;
    ProgressDialog progressDialog;
    //private HashMap<String, Integer> readTag = new HashMap<String, Integer>();
    private long lastTimeBackPressed;
    private WebView webView;
    private WebView childView;
    private WebBridge webBridge;
    private LinearLayout mainLayout;
    /*private final int versionCode = com.mojix.java_android_coway_wtms_mobile.BuildConfig.VERSION_CODE;*/
    private final String versionName = com.mojix.java_android_coway_wtms_mobile.BuildConfig.VERSION_NAME;
    private final String apkName = "CowayMobile.apk";

    /*** 운영변경 ***/
    //private String serverURL = "http://220.95.239.46:8080/"; // 개발서버
    private String serverURL = "http://220.95.239.56:8080/"; // 운영서버
    //private String serverURL = "http://192.168.123.102:8082/"; // 개발
    //private String serverURL = "http://192.168.55.204:8082/"; // 개발



    private List<String> startURL = new ArrayList<String>();;
    private Animation tranlateShow;
    private Animation tranlateGone;



    private boolean isbarcodeViewShow = false;
    String myURL = "";
    String childURL = "";

    private boolean isCooldownTime = false;
    private ZXingScannerView mScannerView;
    private ViewGroup contentFrame;

   /* private String reverseGeocode(double lat, double lng) {
        Geocoder gc = new Geocoder(getBaseContext(), Locale.getDefault());
        String ret = "";
        List<Address> addr = null;
        try {
            addr = gc.getFromLocation(lat, lng, 2);
        } catch (IOException e) {
            Log.e("YS","Geocoder I/O Exception", e);
        }

        Log.i("YS", "addr:" + addr.get(1).getAddressLine(0));
        return addr.get(1).getAddressLine(0);
    }*/

    @Override
    public void handleResult(Result rawResult) {

        Double lat = GpsTracker.getInstance().getLatitude();
        Double lng = GpsTracker.getInstance().getLongitude();

        Log.i("YS", "GpsTracker.getInstance()1111");
        //webBridge.sendScript("javascript:setNaviAddress(\"" + lat +  "\", \"" + lng + "\");");
        webBridge.sendScript("javascript:scanQR(\"" + rawResult.getBarcodeFormat().toString() + "\", \"" + rawResult.getText() + "\", \"" + lat + "\", \"" + lng + "\");");
        mScannerView.resumeCameraPreview(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mScannerView = null;
        completelyAppClose();
    }

    private void completelyAppClose(){
        moveTaskToBack(true);						// 태스크를 백그라운드로 이동
        finishAndRemoveTask();						// 액티비티 종료 + 태스크 리스트에서 지우기
        android.os.Process.killProcess(android.os.Process.myPid());	// 앱 프로세스 종료
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initAppView();
        initSlideView(true);
        if (savedInstanceState != null)
            ((WebView)findViewById(R.id.webview)).restoreState(savedInstanceState.getBundle("webViewState"));
        MyPermission.checkPermission(this);
        GpsTracker.getInstance().initGPS(this);
    }

    private void initAppView(){
        mainLayout = findViewById(R.id.main_layout);
        initBarcodeView();
        webView = (WebView) findViewById(R.id.webview);
        setWebViewInit(webView);
        setWebView();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void initBarcodeView(){

        //mScannerView = (ZXingScannerView) findViewById(R.id.barcode_scanner);
        contentFrame = (ViewGroup) findViewById(R.id.barcode_scanner);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        mScannerView.setResultHandler(this);
        mScannerView.setAutoFocus(true);
        contentFrame.addView(mScannerView);
    }

    private void initSlideView(boolean isPortrait){
        tranlateShow = null;
        tranlateGone = null;
        if (isPortrait){
            tranlateShow = AnimationUtils.loadAnimation(this,R.anim.slide_show_y);
            tranlateGone = AnimationUtils.loadAnimation(this,R.anim.slide_gone_y);
            SlidingPageAnimationListener animListener = new SlidingPageAnimationListener();
            tranlateShow.setAnimationListener(animListener);
            tranlateGone.setAnimationListener(animListener);
        }else{
            tranlateShow = AnimationUtils.loadAnimation(this,R.anim.slide_show_x);
            tranlateGone = AnimationUtils.loadAnimation(this,R.anim.slide_gone_x);
            SlidingPageAnimationListener animListener = new SlidingPageAnimationListener();
            tranlateShow.setAnimationListener(animListener);
            tranlateGone.setAnimationListener(animListener);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle bundle = new Bundle();
        webView.saveState(bundle);
        outState.putBundle("webViewState", bundle);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {

        super.onConfigurationChanged(newConfig);
//        setContentView(R.layout.activity_main);
//        initAppView();

        mScannerView.stopCamera();
        initBarcodeView();
        contentFrame.setVisibility(View.GONE);
        isbarcodeViewShow = false;

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            mainLayout.setOrientation(LinearLayout.VERTICAL);
            initSlideView(true);
        }else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            mainLayout.setOrientation(LinearLayout.HORIZONTAL);
            initSlideView(false);
        }
    }

    private void setWebViewInit(WebView webView){

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);

        WebSettings settings = webView.getSettings();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        // CORS 처리 시작 (파일 업로드)
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setAllowUniversalAccessFromFileURLs(true);
        // CORS 처리 끝

        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setJavaScriptEnabled(true);        // Javascript 사용하기
        settings.setBuiltInZoomControls(true);      // WebView 내장 줌 사용여부
        settings.setLoadWithOverviewMode(true);     // 화면에 맞게 WebView 사이즈를 정의
        settings.setUseWideViewPort(true);          // ViewPort meta tag를 활성화 여부
        settings.setDisplayZoomControls(false);     // 줌 컨트롤 사용 여부
        settings.setSupportZoom(true);             // 사용자 제스처를 통한 줌 기능 활성화 여부
        settings.setDefaultTextEncodingName("UTF-8");  // TextEncoding 이름 정의
        settings.setSupportMultipleWindows(true);   // 여러개의 윈도우를 사용할 수 있도록 설정
        settings.setLoadsImagesAutomatically(true); // 웹뷰가 앱에 등록되어 있는 이미지 리소스를 자동으로 로드하도록 설정
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE); // 웹뷰 캐시 사용 여부 설정
        settings.setAppCacheEnabled(true);
        settings.setLoadsImagesAutomatically (true) ;     //웹뷰가 앱에 등록되어 있는 이미지 리소스를 자동으로 로드하도록 설정하는 속성입니다.
        settings.setDatabaseEnabled(true);
        settings.setDomStorageEnabled(true);         // Setting Local Storage
    }

    private void setWebView(){
        //progressDialog = new ProgressDialog(MainActivity.this , AlertDialog.THEME_DEVICE_DEFAULT_DARK) ;
        webView.setWebViewClient(new ExWebViewClient(this, webView, childView, childURL, progressDialog));
        webView.setWebChromeClient(new ExWebChromeClient(this, webView, childView, childURL));
        webBridge = new WebBridge(webView);
        webView.addJavascriptInterface(webBridge, "Android");
        webView.setNetworkAvailable(true);
        webView.setDownloadListener(new DownloadListener() {

            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
                Log.i("ys", "onDownloadStart" +  url + ", "  +  contentDisposition + ", " +  mimeType + ", " +  contentLength);
                Uri downloadUri = null;
                try {
                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                    request.setMimeType(mimeType);
                    request.addRequestHeader("User-Agent", userAgent);
                    request.setDescription("Downloading file");

                   /* String fileName = contentDisposition.replace("inline; filename=", "");
                    fileName = fileName.replaceAll("\"", "");*/
                    request.setTitle(apkName);
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, apkName);
                    Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS).mkdirs();

                    DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                    dm.enqueue(request);
                    downloadUri = Uri.parse(url);
                    Toast.makeText(getApplicationContext(), "다운로드 완료 후 CowayMobile.apk 파일을 설치해 주십시오", Toast.LENGTH_LONG).show();
                } catch (Exception e) {

                    if (ContextCompat.checkSelfPermission(MainActivity.this,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            Toast.makeText(getBaseContext(), "첨부파일 다운로드를 위해\n동의가 필요합니다.", Toast.LENGTH_LONG).show();
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    110);
                        } else {
                            Toast.makeText(getBaseContext(), "첨부파일 다운로드를 위해\n동의가 필요합니다.", Toast.LENGTH_LONG).show();
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    110);
                        }
                    }
                }
            }
        });

        webView.loadUrl(serverURL);
    }

   /* private BroadcastReceiver completeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Resources res = context.getResources();

            Toast.makeText(context, "다운로드 완료", Toast.LENGTH_LONG).show();

            startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));

                *//*Uri apkUri = Uri.fromFile(f);
                intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
                startActivity(intent);*//*
        }

    };*/

    private String getRealPathFromURI(Uri contentUri) {
        if (contentUri.getPath().startsWith("/storage")) {
            return contentUri.getPath();
        }

        String id = DocumentsContract.getDocumentId(contentUri).split(":")[1];
        String[] columns = { MediaStore.Files.FileColumns.DATA };
        String selection = MediaStore.Files.FileColumns._ID + " = " + id;
        Cursor cursor = getContentResolver().query(MediaStore.Files.getContentUri("external"), columns, selection, null, null);
        try {
            int columnIndex = cursor.getColumnIndex(columns[0]);
            if (cursor.moveToFirst()) {
                return cursor.getString(columnIndex);
            }
        } finally {
            cursor.close();
        }
        return null;
    }


//    public String getRealPathFromURI(Uri contentUri) {
//
//        String[] proj = { MediaStore.Images.Media.DATA };
//
//        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
//        cursor.moveToNext();
//        String path = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DATA));
//        Uri uri = Uri.fromFile(new File(path));
//
//        cursor.close();
//        return path;
//    }

    private int getRotate(Uri uri){
        ExifInterface exif = null;

        try {
            exif = new ExifInterface(getRealPathFromURI(uri));
        } catch (IOException e) {
            e.printStackTrace();
        }

        int exifOrientation;
        int exifDegree;

        if (exif != null) {
            exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            exifDegree = CameraUtil.exifOrientationToDegrees(exifOrientation);
        } else {
            exifDegree = 0;
        }

        return exifDegree;
    }

    //액티비티가 종료될 때 결과를 받고 파일을 전송할 때 사용 (웹앱 클래스에서 컨텍스트 가져와서 결과값 넣어줌)
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode)
        {
            case FILECHOOSER_NORMAL_REQ_CODE:
                if (resultCode == RESULT_OK)
                {
                    if (filePathCallbackNormal == null) return;
                    Uri result = (data == null || resultCode != RESULT_OK) ? null : data.getData();

                    if (result != null){
                        //result = CameraUtil.getImageUri(this, CameraUtil.rotate(CameraUtil.resize(this, result,50), getRotate(result))) ;
                        result = CameraUtil.getImageUri(this, CameraUtil.rotate(CameraUtil.resize(this, result,50), 90)) ;
                    }
                    //  onReceiveValue 로 파일을 전송한다.
                    filePathCallbackNormal.onReceiveValue(result);
                    //filePathCallbackNormal.onReceiveValue(result);
                    filePathCallbackNormal = null;
                }
                break;
            case FILECHOOSER_LOLLIPOP_REQ_CODE:
                if (resultCode == RESULT_OK)
                {
                    if (filePathCallbackLollipop == null) return;
                    if (data == null){
                        data = new Intent();
                    }

                    if (data.getData() == null){
                        //data.setData(CameraUtil.getImageUri(this, CameraUtil.rotate(CameraUtil.resize(this, cameraImageUri,50), getRotate(cameraImageUri))));
                        data.setData(CameraUtil.getImageUri(this, CameraUtil.rotate(CameraUtil.resize(this, cameraImageUri,50), 90)));
                    }

                    filePathCallbackLollipop.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, data));
                    filePathCallbackLollipop = null;
                }
                else
                {
                    if (filePathCallbackLollipop != null)
                    {   //  resultCode에 RESULT_OK가 들어오지 않으면 null 처리하지 한다.(이렇게 하지 않으면 다음부터 input 태그를 클릭해도 반응하지 않음)
                        filePathCallbackLollipop.onReceiveValue(null);
                        filePathCallbackLollipop = null;
                    }

                    if (filePathCallbackNormal != null)
                    {
                        filePathCallbackNormal.onReceiveValue(null);
                        filePathCallbackNormal = null;
                    }
                }
                break;
            default:

                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void resetCoolTime(final int ms){
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        isCooldownTime = false;
                    }
                },
                ms
        );
    }

    private void showScanner(boolean visiblity){
        // 안보여있었다면 보이게
        mainLayout.startAnimation(visiblity ?  tranlateGone : tranlateShow);
        // 현재 상태 변경
        isbarcodeViewShow = !visiblity;
    }

    public void doScan(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable(){
                    @Override
                    public void run() {
                        showScanner(isbarcodeViewShow);
                    }
                });
            }
        }).start();
    }

    private class SlidingPageAnimationListener implements Animation.AnimationListener {
        @Override public void onAnimationStart(Animation animation)
        {

        }
        public void onAnimationEnd(Animation animation)
        {
            if (isbarcodeViewShow){
                mScannerView.startCamera();
                contentFrame.setVisibility(View.VISIBLE);
            }else{
                mScannerView.stopCamera();
                contentFrame.setVisibility(View.GONE);
            }
        }
        @Override public void onAnimationRepeat(Animation animation)
        {

        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        if(webView != null)
            webView.resumeTimers();


        //mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume

        //barcodeView.resume();
        disposableObserver = getObserver() ;
        RxEventBus.getInstance().getEvent().subscribe(disposableObserver);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(webView != null)
            webView.pauseTimers();
        mScannerView.stopCamera();           // Stop camera on pause
        //barcodeView.pause();
        disposableObserver.dispose();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return mScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    private void setStartURL(String[] urlArray){
        startURL = new ArrayList<String>();
        startURL.add(serverURL);
        for (String url: urlArray) {
            startURL.add(serverURL + url);
        }
    }

    private DisposableObserver<? super Object> getObserver() {
        return new DisposableObserver<Object>() {
            @Override
            public void onNext(Object o) {
                RxEventItem data = (RxEventItem) o;

                Log.i("YS", "DisposableObserver: " + data.sender);
                switch (data.sender) {
                    case RxEventMapper.ClOSE_ACTIVITY:
                        new MaterialDialog.Builder(getApplicationContext()).title((String)data.obj).positiveText("확인").show();
                        MainActivity.this.finish();
                        break;
                    case RxEventMapper.myURL:
                        myURL = (String)data.obj;
                        break;
                    case RxEventMapper.doScan:
                        doScan();
                        break;
                    case RxEventMapper.webPageLoaded:
                        if (isbarcodeViewShow){
                            showScanner(isbarcodeViewShow);
                        }
                    break;
                    case RxEventMapper.setStartURL:
                        setStartURL(((String)data.obj).split(","));
                        break;
//                    case RxEventMapper.settingAppConst:
//                        String[] constArray;
//                        try{
//                            constArray = ((String)data.obj).split(",");
//                            if (constArray.length == 3){
//                                startURL1 = serverURL + constArray[0];
//                                startURL2 = serverURL + constArray[1];
//                                scanCooldownTime = Integer.parseInt(constArray[2]);
//                            }
//                        }catch (Exception e){
//                            System.out.println(e);
//                        }
//                    break;
                    case RxEventMapper.getAppVersion:
                        webBridge.sendScript("javascript:getAppVersion(" + versionName + ", 'Mobile');");
                        break;
                    case RxEventMapper.insertManualData:
                        Double lat = GpsTracker.getInstance().getLatitude();
                        Double lng = GpsTracker.getInstance().getLongitude();

                        Log.i("YS", "insertManualData GpsTracker.getInstance() " + lat + ", " + lng);
                        //webBridge.sendScript("javascript:setNaviAddress(\"" + lat +  "\", \"" + lng + "\");");
                        String qrCode = (String)data.obj;
                        webBridge.sendScript("javascript:scanQR(\"\", \"" + qrCode + "\", \"" + lat + "\", \"" + lng + "\");");
                        break;
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e("TAG", "MainActivity DisposableObserver onNext: " + e.getMessage());
            }

            @Override
            public void onComplete() {
                Log.e("COMPLETE", "COMPLETE");
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (grantResults.length > 0  && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // 허용시
            // permission was granted, yay! Do the
            // contacts-related task you need to do.
        } else {
            AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
            localBuilder.setTitle("권한 설정")
                    .setMessage("권한 거절로 인해 일부기능이 제한됩니다.")
                    .setPositiveButton("권한 설정하러 가기", new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt){
                            try {
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                        .setData(Uri.parse("package:" + getPackageName()));
                                startActivity(intent);
                            } catch (ActivityNotFoundException e) {
                                e.printStackTrace();
                                Intent intent = new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                                startActivity(intent);
                            }
                        }})
                    .setNegativeButton("취소하기", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
                            Toast.makeText(getApplication(),"권한 취소",Toast.LENGTH_SHORT).show();
                        }})
                    .create()
                    .show();

        }
        return;
    }

    @Override
    public void onBackPressed() {
        if (startURL.size() == 0){
            finish();
        }

        if (startURL.contains(webView.getUrl()) && (childURL.equals("") || childURL.equals(null) || childURL.isEmpty())) {
            if (System.currentTimeMillis() - lastTimeBackPressed < 2000) {
                finish();
                return;
            }
            Toast.makeText(this, "'뒤로' 버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
            lastTimeBackPressed = System.currentTimeMillis();
        } else if (webView.canGoBack() && (childURL.equals("") || childURL.equals(null) || childURL.isEmpty())) {
            webView.goBack();
        } else if(!childURL.equals("") || !childURL.equals(null) || !childURL.isEmpty()) {
            webView.removeView(childView);
            childURL="";
            webView.reload();
        }
    }
}
