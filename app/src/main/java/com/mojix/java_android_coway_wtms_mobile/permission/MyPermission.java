package com.mojix.java_android_coway_wtms_mobile.permission;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.mojix.java_android_coway_wtms_mobile.R;
import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventBus;
import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventItem;
import com.mojix.java_android_coway_wtms_mobile.rxJava.RxEventMapper;

import java.util.ArrayList;

import static com.mojix.java_android_coway_wtms_mobile.MainActivity.REQUEST_PERMISSION_CODE;

public class MyPermission {
    @SuppressLint("NewApi")
    //퍼미션 체크
    public static void checkPermission(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            final String[] permissionList = new String[]{ //체크할 퍼미션 리스트 넣어줌
                    Manifest.permission.INTERNET,
                    Manifest.permission.CAMERA,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
//                    Manifest.permission.READ_PHONE_STATE,
            };

            ArrayList<String> permissions = new ArrayList<String>();
            int grantResult = PackageManager.PERMISSION_GRANTED;
            for (int i = 0; i < permissionList.length; i++) {
                grantResult = ContextCompat.checkSelfPermission(activity.getApplicationContext(), permissionList[i]);
                if (grantResult == PackageManager.PERMISSION_DENIED) {
                    permissions.add(permissionList[i]);
                }
            }

            if (permissions.size() > 0) {
                String[] requestPermissions = new String[permissions.size()];
                ActivityCompat.requestPermissions(activity, permissions.toArray(requestPermissions), REQUEST_PERMISSION_CODE);
            }
        }
    }
}
